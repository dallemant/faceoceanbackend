# README #


### What is this repository for? ###

* back end FaceOcean pour ajout de la fonction autopilot en mode Vent Réel
* 0.0.0


# Dependencies #
* nodejs 4.3.0

* voir le fichier [package.json](./package.json)

# Summary of set up #
## sous raspberrypi A, B, B+ ##

1. installation de nodejs
```
#!bash
cd /tmp
wget https://nodejs.org/dist/v4.3.0/node-v4.3.0-linux-armv6l.tar.gz
tar -xvf node-v4.3.0-linux-armv6l.tar.gz
cd node-v4.3.0-linux-armv6l
sudo cp -R * /usr/local/
npm cache clean
```
[source](http://blog.wia.io/installing-node-js-v4-0-0-on-a-raspberry-pi/)

2. installation des sources
```
#!bash
sudo apt-get update
sudo apt-get install git
mkdir ~/FaceOceanBackend
cd ~/FaceOceanBackend
git clone https://dallemant@bitbucket.org/dallemant/faceoceanbackend.git
npm install
npm install -g bower
bower install
npm start
```

## sous windows ##

1. installation de nodejs 4.3.0 et git-scm

2. installation des sources depuis git-bash
```
#!bash
mkdir ~/FaceOceanBackend
cd ~/FaceOceanBackend
git clone https://dallemant@bitbucket.org/dallemant/faceoceanbackend.git
npm install
npm install -g bower
bower install
NO_WIRING_PI=1 npm start
```

## Configuration ##

*point d'entree  : [navigation_api.js](routes/navigation_api.js)

*la logique du backend est rassemblée dans: [face_ocean.js](face_ocean.js)
