function AsservTrueWind(){
  this.param1 = 0.0;
  this.param2 = 0.0;  
  this.time= process.hrtime();
}

AsservTrueWind.prototype.reset = function(){
   this.param1 = 0.0;
   this.param2 = 0.0;  
   this.time= process.hrtime();   
}

AsservTrueWind.prototype.init = function(conf){
 var p1 = this.param1;
 this.param1 = conf.param1 || p1;
 var p2 = this.param2;
 this.param2 = conf.param2 || p2; 
 
}

AsservTrueWind.prototype.printParams = function(){
 console.log("["+ this.param1 + ";"+ this.param2+"]");   
}

AsservTrueWind.prototype.getElapsedMsSinceLastCall = function(){     
    var delta = process.hrtime(this.time);
    this.time= process.hrtime();   
    return (delta[0]*1000.0)+ (delta[1]/1000000.0);
}

// twa_setpoint :consigne actue
AsservTrueWind.prototype.Process= function( cog, true_wind_dir_deg, delta_t_ms,callback ){
    
    console.log("Process inputs: "+ ',' + cog+ ',' + true_wind_dir_deg+ ',' + delta_t_ms);
    
    //all magics append here!    
    
    callback(null,{setpoint_diff_deg:"+1",setpoint_abs_deg:"2"});
}

module.exports = AsservTrueWind;


if (require.main === module) {
    
    var algo = new AsservTrueWind;
    console.log('init ms = ' + algo.getElapsedMsSinceLastCall());
    setInterval(function(){
        console.log('elapsed ms = ' + algo.getElapsedMsSinceLastCall());
    },1000);    
}