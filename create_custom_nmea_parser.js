var util = require('util'),
    nmea = require('nmea-0183');

//console.log(util.inspect(nmea));

//var sentence = '$GPGGA,123519,4807.038,N,01131.324,E,1,08,0.9,545.4,M,46.9,M, , *42';
//var sentence = '$IIMWV,017,R,02.91,N,A*2F';
//var sentence = '$IIHDG,318.9,,,,*4A\r\n';
var sentence = '18.9,,,,*4A\r\n';


var parseFloatX = function(f) {
    if(f === '') {
        return 0.0;
    }
    return parseFloat(f);
}


/** custom IIHDG decoder */
var Decoder = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var hdg;
        if(tokens.length < 5) {
            throw new Error('HDG : not enough tokens');
        }

        // trim whitespace
        // some parsers may not want the tokens trimmed so the individual parser has to do it if applicable
        for( i = 0; i < tokens.length; ++i) {
            tokens[i] = tokens[i].trim();
        }
        hdg = {
            id : tokens[0].substr(1),
            head : parseFloatX(tokens[1]),
            w : parseFloatX(tokens[2]),
            x : parseFloatX(tokens[3]),
            y : parseFloatX(tokens[4]),
            z : parseFloatX(tokens[5]),
           
        };

        return hdg;
    };
};

//instancier le parser et l'ajouter à la liste des parser
var dec = new Decoder('IIHDG');
nmea.addParser(dec);
//todo intercepter les erreur de parse dans un TRY/CATCH
var gga = nmea.parse(sentence);

console.log(util.inspect(gga));
console.log(gga.head);

if (require.main === module) {
 
}