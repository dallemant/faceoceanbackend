var util = require('util');

function FaceOcean() {   
  
    // module de decodage des trames nmea
    var nmea_decoder = null;
    //algorithme d'asservissement
    var algo = null;  
    //donnees de navigations
    var nav_data = {};
    //controle des gpio
    var actuator = null;

    var asserv_timer_id = null;

    return {
      
        //injection des dependances
        use: function (nmea_decoder_, asserv_algo_, tcp_, gpio_actuator_) {
            nmea_decoder = nmea_decoder_;            
            algo = asserv_algo_ ;
            actuator = gpio_actuator_;
            this.addTcpLineEmiter(tcp_);
        },

        startAsserv: function () {
           
            if (asserv_timer_id) { return; }
            algo.reset();
            asserv_timer_id = setInterval(function () {   
                                     
                //TODO: utiliser les bonnes donnees de nav
                algo.Process(nav_data.twa_setpoint_status,
                    nav_data.cog_deg,
                    nav_data.true_wind_dir_deg,
                    algo.getElapsedMsSinceLastCall(),
                    function (err, ret) {
                        if (err) {
                            return;
                        }
                        actuator.applySetpointDiff(ret.setpoint_diff_deg);
                    });
            }, 2000);
        },

        stopAsserv : function () {
            if (asserv_timer_id) {
                clearInterval(asserv_timer_id);
                asserv_timer_id = null;
            }
        },

        // possible decoder plusiers canaux
        addTcpLineEmiter: function (tcp) {

            tcp.on('connect', function () {
                console.log("event tcp connect " + tcp.isConnected().toString());
            });

            tcp.on('line', function (data) {
                //mise a jour des donnees de nav
                nav_data = nmea_decoder.updateNavData(data, nav_data);
            });

            tcp.on('error', function (error) {
                console.error("error " + error.toString());
            });
    
            //demarrage inconditionnel de l'acquisition des donnees nmea
            tcp.start();
        },

        //accesseur aux donnees de nav
        getNavigationData: function () {            
            return nav_data;
        }
    };
}







//singleton
module.exports = new FaceOcean; 
