
function GpioActuator(){
    
    this.gpio={}
    this.repeat = 0;
    this.timer_id = undefined;
    this.pulse_length_ms = 500;   
   
}

GpioActuator.prototype.setIntervalBetweenPress = function(interval_ms){
    this.pulse_length_ms = interval_ms;
}

GpioActuator.prototype.setGpio = function(io){
    this.gpio = io;
}

GpioActuator.prototype.isRunning = function(){
    return this.timer_id !== undefined;
}

GpioActuator.prototype.applySetpointDiff = function(target){
    if (this.isRunning()){
        return false;
    }
    //utilise seulement +1/-1
    this.repeat = Math.abs(target*2);
    var that = this;   
    
    this.timer_id = setInterval(function () {
        if( target > 0){   that.gpio.switchStatePlus();
        }else{             that.gpio.switchStateMinus(); }
        that.repeat --;
        if(that.repeat === 0){
            clearInterval(that.timer_id);
            that.timer_id = -1;
            that.gpio.reset();
        }
    }, this.pulse_length_ms);
    
    return true;
}


module.exports =  GpioActuator;