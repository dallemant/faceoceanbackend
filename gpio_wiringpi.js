var util = require('util');
var wpi = undefined;
if(process.env.NO_WIRING_PI == undefined) {
    try{
    wpi  = require('wiring-pi');    
    }catch(err){
        console.error("**********\nchargement du module wiring-pi echoue\n ajouter la variable d'environnement NO_WIRING_PI=1 pour se passer du module\n************ ")
        throw err;
    }
}

function Gpio(){    
    this.pinUp=7;
    this.pinDown=0;
    this.valueUp=0;
    this.valueDown=0;
    
    if(wpi) {        
        wpi.setup('wpi');
        var info = wpi.piBoardId();
        console.log(util.inspect(info));

        console.log(wpi.PI_MODEL_NAMES[info.model]);
        console.log(wpi.PI_REVISION_NAMES[info.rev]);
        console.log(wpi.PI_MAKER_NAMES[info.marker]);
        
        wpi.pinMode(this.pinUp, wpi.OUTPUT);
        wpi.pinMode(this.pinDown, wpi.OUTPUT);
        //TODO mettre dans le bonne etat (haut ou bas)
        this.reset();
        
    } else {  
        console.log("gpio not supported");        
    }   
    
    
    console.log(util.inspect(this));    
    
    
}

Gpio.prototype.init = function(){
    
    
}

Gpio.prototype.reset = function(){
     console.log("reset");
    this.valueUp= 0;
    this.valueDown=0;
    
    if(wpi ){
        wpi.digitalWrite(this.pinUp,0);
        wpi.digitalWrite(this.pinDown,0);
    }
}

Gpio.prototype.switchStatePlus = function(){
    this.valueUp = +!this.valueUp;    
    if(wpi){        
        wpi.digitalWrite(this.pinUp,this.valueUp);        
    }    
}

Gpio.prototype.switchStateMinus = function(){
   this.valueDown = +!this.valueDown;
   if(wpi ){
        wpi.digitalWrite(this.pinDown, this.valueDown);
   }    
}

module.exports =  Gpio;