var prefix = './node_modules/nmea-0183/lib/';
var RMC = require(prefix +"codes/RMC.js");
var VTG = require(prefix +"codes/VTG.js");
var MWV = require(prefix +"codes/MWV.js");
var nmea_helper = require(prefix +'Helper.js');
var util = require("util");
var nmea  = require('nmea-0183');

console.log(util.inspect(RMC));

nmea.addParser(new MWV.Decoder('IIMWV'));
nmea.addParser(new RMC.Decoder('IIRMC'));
nmea.addParser(new VTG.Decoder('IIVTG'));



var HDG = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var hdg;
        if(tokens.length < 3 ) {
            throw new Error('HDG/HDM : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < 2; ++i) {
            tokens[i] = tokens[i].trim();
        }
        hdg = {
            id : tokens[0].substr(1),
            head_magnetic_deg : nmea_helper.parseFloatX(tokens[1]),                   
        };
        return hdg;
    };
};

var MTW = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var mtw;
        if(tokens.length < 3 ) {
            throw new Error('MTW: not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < 2; ++i) {
            tokens[i] = tokens[i].trim();
        }
        mtw = {
            id : tokens[0].substr(1),
            water_temp_c_deg : nmea_helper.parseFloatX(tokens[1]),                   
        };
        return mtw;
    };
};

var MWD = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var mwd;
        if(tokens.length < 8) {
            throw new Error('MWD : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
        mwd = {
            id : tokens[0].substr(1),
            wind_dir_true_deg : nmea_helper.parseFloatX(tokens[1]),
            wind_dir_mag_deg  : nmea_helper.parseFloatX(tokens[3]),
            wind_speed_kts  : nmea_helper.parseFloatX(tokens[5]),
            wind_speed_m_s  : nmea_helper.parseFloatX(tokens[7])                   
        };
        return mwd;
    };
};

var VHW = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var vhw;
        if(tokens.length < 8) {
            throw new Error('VHW : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
        vhw = {
            id : tokens[0].substr(1),
            compass_true_deg : nmea_helper.parseFloatX(tokens[1]),
            compass_mag_deg  : nmea_helper.parseFloatX(tokens[3]),
            surface_speed_kts  : nmea_helper.parseFloatX(tokens[5]),
            surface_speed_kph  : nmea_helper.parseFloatX(tokens[7])                             
        };
        return vhw;
    };
};

var VLW = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var vlw;
        if(tokens.length < 5 ){
            throw new Error('VLW : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
        vlw = {
            id : tokens[0].substr(1),
            log_total_miles : nmea_helper.parseFloatX(tokens[1]),
            log_daily_miles  : nmea_helper.parseFloatX(tokens[3])                                       
        };
        return vlw;
    };
};

var VWR = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var vwr;
        if(tokens.length < 8 ){
            throw new Error('VWR : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
       
        vwr = {
            id : tokens[0].substr(1),
            wind_speed_apparent_kts : nmea_helper.parseFloatX(tokens[3]),
            wind_speed_apparent_m_s : nmea_helper.parseFloatX(tokens[5]),
            wind_speed_apparent_kph : nmea_helper.parseFloatX(tokens[7]),                                             
        };
        
         if(tokens[2] == 'R'){
             vwr.wind_dir_apparent_starboard_deg = nmea_helper.parseFloatX(tokens[1]);
         }else{
             vwr.wind_dir_apparent_port_deg = nmea_helper.parseFloatX(tokens[1]);
         }
        
        return vwr;
    };
};

var VWT = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var vwt;
        if(tokens.length < 8 ){
            throw new Error('VWR : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
       
        vwt = {
            id : tokens[0].substr(1),
            wind_speed_true_kts : nmea_helper.parseFloatX(tokens[3]),
            wind_speed_true_m_s : nmea_helper.parseFloatX(tokens[5]),
            wind_speed_true_kph : nmea_helper.parseFloatX(tokens[7]),                                             
        };
        
         if(tokens[2] == 'R'){
             vwt.wind_dir_true_starboard_deg = nmea_helper.parseFloatX(tokens[1]);
         }else{
             vwt.wind_dir_true_port_deg = nmea_helper.parseFloatX(tokens[1]);
         }
        
        return vwt;
    };
};

var NotHandled = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var vpw;
        if(tokens.length < 1 ){
            throw new Error('NotHandled : not enough tokens');
        }        
        vpw = {
            id : tokens[0].substr(1)                                                 
        };
        return vpw;
    };
};


var RMB = function(id) {
    this.id = id;
    this.parse = function(tokens) {
        var i;
        var rmb;
        if(tokens.length < 13 ){
            throw new Error('RMB : not enough tokens');
        }
        // trim whitespace        
        for( i = 0; i < tokens.length ; ++i) {
            tokens[i] = tokens[i].trim();
        }
        rmb = {
            id : tokens[0].substr(1),
            waypoint_dist_miles : nmea_helper.parseFloatX(tokens[10]),
            waypoint_heading_true_deg  : nmea_helper.parseFloatX(tokens[11]),
            waypoint_speed_to_kts  : nmea_helper.parseFloatX(tokens[12])                                            
        };
        return rmb;
    };
};


//custom
nmea.addParser(new HDG('IIHDG'));
nmea.addParser(new HDG('IIHDM'));
nmea.addParser(new MWD('IIMWD'));
nmea.addParser(new VHW('IIVHW'));
nmea.addParser(new VLW('IIVLW'));
nmea.addParser(new VWR('IIVWR'));
nmea.addParser(new VWT('IIVWT'));
nmea.addParser(new MTW('IIMTW'));
nmea.addParser(new RMB('IIRMB'));
nmea.addParser(new NotHandled('IIVPW'));
nmea.addParser(new NotHandled('IIAPB'));
nmea.addParser(new NotHandled('IIBWC'));
nmea.addParser(new NotHandled('IIGLL'));


function NmeaDecoder() {    
}
NmeaDecoder.prototype.parse = function(sentence){
    return nmea.parse(sentence);
}

NmeaDecoder.prototype.updateNavData = function(sentence, nav_data){
    try {
        
        var msg = nmea.parse(sentence);
        //TODO: refaire une passe sur tous les message 
        // pour gerer les ecrasement
        
        //gestion des message contenant plusieurs informations
        if(msg.id == 'IIMWV'){
            //renomme l'id du message pour conserver les 2
            msg.id += msg.reference;
            delete msg.reference;
        }
        
        var id = msg.id;
        delete msg.id;
        var cpt = 0;
        if (nav_data[id]) {
            cpt = nav_data[id].counter;
        }
        nav_data[id] = msg;
        nav_data[id].counter = cpt+1;

    } catch (err) {
    }
    
    return nav_data;

}

module.exports = new NmeaDecoder();