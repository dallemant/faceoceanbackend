/* Simple Hello World in Node.js */
console.log("Hello World");
var GpioCtrl = require('./gpio_actuator');
var Gpio = require('./gpio_wiringpi');
var ctrl = new GpioCtrl();

//interface
var gpio = 
{
    switchStatePlus:function(){
        console.log("switch +");
    },
    switchStateMinus:function(){
        console.log("switch -");
    },
    reset: function(){
        console.log("reset");
    }
}

gpio = new Gpio;

ctrl.setGpio(gpio);
ctrl.setIntervalBetweenPress(2000);

ctrl.applySetpointDiff(2);