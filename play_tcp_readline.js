var Tcp = require('./tcp_readline.js'),
    nmea = require('./nmea_decoder.js');
var tcp = new Tcp();
var util = require('util');

tcp.init('127.0.0.1', 12345);

tcp.on('connect', function () {
    console.log("1 event tcp connect " + tcp.isConnected().toString());
});

tcp.on('line', function (data) {
    console.log("line " + data);
    try {
        var msg = nmea.parse(data);
        console.log(util.inspect(msg));
    } catch (err) {
        console.error("nmea parse error " + err.toString());
    }
});

tcp.on('error', function (error) {
    console.error("error " + error.toString());
});


tcp.start();
function toto(){
setTimeout(function () {
    console.log("force deconnxion");
    tcp.stop();
    setTimeout(function () {
        console.log("force reconnexion");
        tcp.start();
    }, 5000);
}, 5000);
}