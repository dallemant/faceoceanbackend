var backend_prefix = 'http://127.0.0.1:3000/api';

var setDefaultValues = function(){
	$(".from-back").text("---");
}; 

var getCurrentSelectedMode = function(){
	var currentSelected = $("#off.active,#true_wind.active,#heading.active");
	if(currentSelected){
		return currentSelected.attr('data-mode');
	}
	return null;
}

var onPlusMinusClicked = function(){
	console.log("Plus / Moins clicked " + $(this).attr("data-increment"));	
	var increment = Number($(this).attr("data-increment"));
	changeConsigne(increment);
}

var onModeChangeClicked = function(){	
	changeConsigne(0, $(this).attr('data-mode'));
}

var changeConsigne = function(increment, dataMode){	
	var cons = Number($("#cap_cons").val());
	cons = cons + increment;
	console.log("Nouvelle consigne : " + cons);
	console.log("Current mode : " + getCurrentSelectedMode());
	var mode;
	if(dataMode) mode = dataMode;
	else mode = getCurrentSelectedMode();
	if(mode){
		var data;
		if(mode=='off'){
			data = {
				'mode': mode
			}
		}else{
			data = {
				'mode': mode,
				'consigne': cons
			}
		}
		$.ajax({
			type:"POST",
			url: backend_prefix+'/control_cons',
			data: data
		}).done(function(){updateState});
	}else{
		console.log("Unknow mode !");
	}
}
var installButtonsHandlers = function(){
	$("#plus_1").click(onPlusMinusClicked);
	$("#plus_10").click(onPlusMinusClicked);
	$("#minus_1").click(onPlusMinusClicked);
	$("#minus_10").click(onPlusMinusClicked);
	
	$("#heading, #true_wind, #off").click(onModeChangeClicked);
}

var prevControlMode = "";
var prevConverged = false;
var prevConsigne = 0;
var setSystemAvailable = function(available){
	$("#general_status").removeClass("glyphicon-ok");
	$("#general_status").removeClass("glyphicon-remove");
	if(available) {
		$("#general_status").addClass("glyphicon-ok");
		$(".must-have-converged").prop('disabled', false);
	}else {
		$("#general_status").addClass("glyphicon-remove");
		$(".must-have-converged").prop('disabled', true);
	}
}

var updateState = function(data){
	// ici data est l'objet échangé avec le back :
	$("#HDG").text(data.heading);
	$("#SOG").text(data.SOG);
	$("#COG").text(data.COG);
	$("#AWA").text(data.AWA);
	$("#TWA").text(data.TWA);
	
	if(prevControlMode!=data.controlMode)
	{
		prevControlMode = data.controlMode;
		$("#off").removeClass("active");
		$("#cap").removeClass("active");
		$("#true_wind").removeClass("active");			
		switch(data.controlMode){
			case "off":
				$("#off").addClass("active");
				break;
			case "cap":
				$("#cap").addClass("active");
				break;
			case "true_wind":
				$("#true_wind").addClass("active");	
				break;
			default:
				console.log(data.controlMode + " is not a known control mode!");
				break;
		}
	}
		
	if(prevConverged != data.estimatorHasConverged){
		prevConverged = data.estimatorHasConverged;
		setSystemAvailable(data.estimatorHasConverged);
	}
	
	if(prevConsigne!=data.currentConsigne){
		prevConsigne = data.currentConsigne;
		$("#cap_cons").val(data.currentConsigne);
	}
}

$(document).ready(function(){
	$("[name=night-day-checkbox]").bootstrapSwitch();
	setDefaultValues();	
	installButtonsHandlers();
	setInterval(function(){$.ajax(backend_prefix+'/current_state').done(updateState)}, 1000);
	
	setSystemAvailable(false);
})