var router = require('express').Router();

var face_ocean_ctrl = require('../face_ocean');
var nmea_decoder = require('../nmea_decoder');

var AsservTrueWind  = require('../asserv_on_true_wind.js');
var asserv_algo = new AsservTrueWind;

var Tcp = require('../tcp_readline.js');
var tcp = new Tcp; 

var GpioCtrl = require('../gpio_actuator');
var gpio_ctrl = new GpioCtrl();

var Gpio = require('../gpio_wiringpi');

var asserv_state = 'off';
var current_consigne = undefined;
// prepare le controleur et lui defini ses dependances
gpio_ctrl.setGpio(new Gpio);//TODO definir les gpio_pinup et gpio_pindown ici
gpio_ctrl.setIntervalBetweenPress(500);
tcp.init('127.0.0.1', 12345);
face_ocean_ctrl.use(nmea_decoder,asserv_algo,tcp,gpio_ctrl);


router.get('/current_state', function(req, res, next) {    
    
    var state = {
        controlMode:asserv_state,
        currentConsigne: current_consigne,
        estimatorHasConverged:	undefined,
        heading: undefined, 
        COG:	 undefined,
        SOG:	 undefined,
        AWA:	 undefined,
        TWA:	 undefined        
    };
    var nav =face_ocean_ctrl.getNavigationData();
    
    //face_ocean_ctrl.startAsserv();
    //return;
    
    if(nav.hasOwnProperty('IIHDG') && nav.IIHDG.hasOwnProperty('head_magnetic_deg')){
        state.heading = nav.IIHDG.head_magnetic_deg;
    }
    
    if(nav.hasOwnProperty('IIRMC') ){
        if(nav.IIRMC.hasOwnProperty('speed')){
            state.SOG = nav.IIRMC.speed;
        }
        
        if(nav.IIRMC.hasOwnProperty('course')){
            state.COG = nav.IIRMC.course;
        }
    }
    
    if(nav.hasOwnProperty('IIVWT') ){
        if(nav.IIVWT.hasOwnProperty('wind_dir_true_starboard_deg')){
            state.TWA = nav.IIVWT.wind_dir_true_starboard_deg;
        }
        if(nav.IIVWT.hasOwnProperty('wind_dir_true_port_deg')){
            state.TWA = - nav.IIVWT.wind_dir_true_port_deg;
        }
    }
    
    if(nav.hasOwnProperty('IIVWR') ){
        if(nav.IIVWR.hasOwnProperty('wind_dir_apparent_starboard_deg')){
            state.AWA = nav.IIVWR.wind_dir_true_starboard_deg;
        }
        if(nav.IIVWR.hasOwnProperty('wind_dir_apparent_port_deg')){
            state.AWA = - nav.IIVWR.wind_dir_true_port_deg;
        }
    }
        
  res.json(state); 
  
  
});

router.post('/control_cons',function(req,res){
    var asserv_by=req.body.mode;// 'true_wind' or 'cap' or 'off'
    var setpoint_deg=req.body.consigne;
    current_consigne = setpoint_deg;

    asserv_state = asserv_by;

    var msg;
    if(asserv_by == "true_wind"){
        
        face_ocean_ctrl.startAsserv();
        msg = 'passe en mode vent réel vers le cap ' + setpoint_deg + "°"; 
    }else if(asserv_by == "cap"){
        face_ocean_ctrl.stopAsserv();
        msg = 'passe en mode GPS vers le cap ' + setpoint_deg + "°"; 
    }else{
        asserv_state = 'off';
        face_ocean_ctrl.stopAsserv();
        msg = "autopilote désactivé !!!";
    }
    res.json({ message: msg });

    //TODO idee : refuser l'asservissement si trop de difference avec le cap en cours

});




module.exports = router;
