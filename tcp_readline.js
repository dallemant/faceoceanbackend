var util = require("util"),
    EventEmitter = require('events').EventEmitter,
    net = require('net'),
    split = require('split');


// definition du module:
// genere les evenement ('connect',bool) et ('line',line) ('error',error)
//decoupe le flux en ligne

function TcpModule() {
    EventEmitter.call(this);
    this.host = '127.0.0.1';
    this.port = 1235;
    this.client = undefined;
    this.is_connected = false;
    this.stop_requested=false;
    this.timeout_id = undefined;    
}
util.inherits(TcpModule, EventEmitter);


TcpModule.prototype.init = function (host, port) {
    this.port = port;
    this.host = host;
}

TcpModule.prototype.stop = function(){
    this.stop_requested=true;
    this.is_connected = false;
    
    if (this.timeout_id != undefined) {
        clearTimeout(this.timeout_id);
        this.timeout_id = undefined;
    }
    if (this.client != undefined) {
        this.client.destroy();
        this.client = undefined;
    }
}

TcpModule.prototype.start = function () {
    this.is_connected = false;
    this.stop_requested=false;
    var that = this;
    this.client = new net.Socket();
    
    //c'est grace au pipe que le flux est decoupé en lignes
    this.client.pipe(split())
        .on('data', function (data) {
            that.emit("line", data);
        })
        .on('error', function (error) {
            that.emit("error", error);
        });


    this.client.connect(this.port, this.host, function () {
        that.is_connected = true;
        that.emit("connect");
    });

    this.client.on('error', function (error) {
        that.is_connected = false;
        that.emit("error", error);
    });

    this.client.on('end', function () {
        that.is_connected = false;
    });

    this.client.on('close', function () {
        that.is_connected = false;
        that.emit("connect");       
        //relance la connection 
        if (!that.stop_requested) {
            that.timeout_id = setTimeout(function () {
                that.timeout_id = undefined;
                that.start()
            }, 5000);
        }
    });
}


TcpModule.prototype.isConnected = function () {
    return this.is_connected;
}

TcpModule.prototype.isStarted = function () {
    return this.client != undefined;
}

module.exports = TcpModule;

if (require.main === module) {
    var tcp = new TcpModule;
    console.log("tcp.isconnected = " + tcp.isConnected())
}