
//line $IIAPB,A,A,0.004,L,N,,,239,T,,239,T,,,A*16
{ id: 'IIAPB' }
//line $IIBWC,,3614.878,N,00637.827,W,239,T,,M,711.558,N,*39
{ id: 'IIBWC' }
//line $IIRMC,160112.00,A,4307.031,N,00555.805,E,000.0,000,060216,001.1,E*5D
{ id: 'IIRMC',
  time: '160112.00',
  valid: 'A',
  latitude: '43.11718333',
  longitude: '5.93008333',
  speed: 0,
  course: 0,
  date: '060216',
  mode: 'E',
  variation: -1.1 }
//line $IIVTG,000,T,,M,000.0,N,,*0C
{ id: 'IIVTG', course: 0, knots: 0, kph: 0, mode: undefined }
//line $IIGLL,4307.031,N,00555.805,E,160112.00,A*1C
{ id: 'IIGLL' }
//line $IIRMB,A,0.004,L,,,,N,,E,711.558,239,,,A*29
{ id: 'IIRMB',
  waypoint_dist_miles: 711.558,
  waypoint_heading_true_deg: 239,
  waypoint_speed_to_kts: 0 }
////line $IIHDG,319.0,,,,*42
{ id: 'IIHDG', head_magnetic_deg: 319 }
//line $IIVWT,013,R,000.5,N,000.2,M,,K*7A
{ id: 'IIVWT',
  wind_speed_true_kts: 0.5,
  wind_speed_true_m_s: 0.2,
  wind_speed_true_kph: 0,
  wind_dir_true_starboard_deg: 13,
  wind_dir_true_port_deg: undefined }
//line $IIHDM,319,M*37
{ id: 'IIHDM', head_magnetic_deg: 319 }
//line $IIMTW,000,C*3D
{ id: 'IIMTW', water_temp_c_deg: 0 }
//line $IIMWD,,T,332,M,000.5,N,000.2,M*71
{ id: 'IIMWD',
  wind_dir_true_deg: 0,
  wind_dir_mag_deg: 332,
  wind_speed_kts: 0.5,
  wind_speed_m_s: 0.2 }
//line $IIMWV,,R,000.5,N,A*16
{ id: 'IIMWV',
  angle: 0,
  reference: 'R',
  speed: 0.5,
  units: 'N',
  status: 'A' }
//line $IIVHW,,T,319,M,00.00,N,,K*40
{ id: 'IIVHW',
  compass_true_deg: 0,
  compass_mag_deg: 319,
  surface_speed_kts: 0,
  surface_speed_kph: 0 }
//line $IIVLW,7552.77,N,0000.00,N*48
{ id: 'IIVLW', log_total_miles: 7552.77, log_daily_miles: 0 }
//line $IIVPW,0.00,N,,M*4C
{ id: 'IIVPW' }
//line $IIVWR,, ,000.5,N,000.2,M,,K*1C
{error:'nmea parse error Error: ERROR:checksum mismatch'}
//line $IIVWT,013,R,000.5,N,000.2,M,,K*7A
{ id: 'IIVWT',
  wind_speed_true_kts: 0.5,
  wind_speed_true_m_s: 0.2,
  wind_speed_true_kph: 0,
  wind_dir_true_starboard_deg: 13 }
